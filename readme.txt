
/**
 * @file
 * readme.txt
 */

#Installation
Download the easypost library from https://github.com/EasyPost/easypost-php & place in the libraries directory with the following path.
sites/all/libraries/easypost/lib/EasyPost

#Setup
Obtain an API key from https://www.easypost.com
